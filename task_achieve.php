<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="none,noindex,nofollow">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/task.css">
    <link href="https://fonts.googleapis.com/css?family=Vollkorn"  rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <title>Complete</title>
  </head>
  <body>
    <div class="header_wrapper">
      <header class="header">
        <div class="logo">
          <a href="index.php"><img src="images/logo.png" alt="Logo"></a>
        </div>
        <div class="login_header">
          <button type="button" name="button" onclick="logout()">ログアウト</button>
          <button type="button" name="button" onclick="login()">ログイン</button>
        </div>
        <nav class="navigation">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="mypage.php">MyPage</a></li>
            <li><a href="ranking.php">Ranking</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
        </nav>
      </header>
    </div>


    <?php
    // データベース操作
    try {
      $db = new PDO('mysql:dbname=hew2020_00820;host=localhost;charset=utf8','hew2020_00820','');
    } catch (PDOException $e) {
      echo 'DB接続エラー ： ' . $e->getMessage();
    }

    // 削除する場合
    if (isset($_POST['delete'])){
      $record = $db -> prepare('DELETE FROM tasks WHERE id=? AND created_at=?');
      $record -> bindParam(1, $_POST['id']);
      $record -> bindParam(2, $_POST['created_at']);
      $check = $record -> execute();
    }
    // 達成管理・レベル更新
    else {
      if ($_POST['flag']){
        $flag = 1;
      } else {
        $flag = 0;
      }
      $datetime = $_POST['created_at'];


      $record = $db -> prepare('UPDATE tasks SET achieve = ? WHERE created_at=?');
      // achieve を 1の時は 0にする
      // 0 の時は 1 にする
      if ($flag==1) {
        $val = 0;
      } else {
        $val = 1;
      }
      $record -> bindParam(1, $val);
      $record -> bindParam(2, $datetime);
      $check =  $record -> execute();

      // タスクを完了したらレベルを上げる
      if (isset($_POST['id'])) {
        // 達成しているタスク数を求める
        $records = $db -> prepare('SELECT COUNT(*) as num FROM tasks WHERE id=? and achieve=1');
        $records -> bindParam(1, $_POST['id']);
        $records -> execute();
        $num = $records -> fetch();
        // レベルを更新
        $record = $db -> prepare('UPDATE users SET level=? WHERE id=?');
        $record -> bindParam(1, $num['num']);
        $record -> bindParam(2, $_POST['id']);
        $lvl_flag = $record -> execute();
        if (!$lvl_flag){
          ?>
          <script>
          alert('エラーが発生しました。申し訳ありませんが、再度お試しください。');
          </script>
          <?php
        }
      }

    }

    // 問題なく処理が終了したらMyPageへ返す
    if ($check){
      header("Location: mypage.php");
      exit();
    } else {
      ?>
      <script>
      alert('エラーが発生しました。申し訳ありませんが、再度お試しください。');
      </script>
      <?php
    }

    ?>



    <script type="text/javascript" src="js/script.js"></script>
    <footer class="footer">
      <div class="social">
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
        <a href="#"><i class="fab fa-youtube"></i></a>
      </div>
      <small>&copy;Complete</small>
    </footer>
  </body>
</html>
