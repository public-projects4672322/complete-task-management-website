function set2fig(num) {
  var ret;
  if (num < 10) {
    ret = "0" + num;
  } else {
    ret = num;
  }
  return ret;
}
function showClock() {
  var nowTime = new Date();
  var nowHour = set2fig(nowTime.getHours());
  var nowMin = set2fig(nowTime.getMinutes());
  var msg = nowHour + ":" + nowMin;
  document.getElementById("realtime").innerHTML = msg;
}
setInterval('showClock()', 1000)
