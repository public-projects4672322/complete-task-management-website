<?php
session_start();
?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="none,noindex,nofollow">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/login.css">
    <link href="https://fonts.googleapis.com/css?family=Vollkorn"  rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Complete</title>
  </head>
  <body>
    <div class="header_wrapper">
      <header class="header">
        <div class="logo">
          <a href="index.php"><img src="images/logo.png" alt="Logo"></a>
        </div>
        <div class="login_header">
          <button type="button" name="button" onclick="logout()">ログアウト</button>
          <button type="button" name="button" onclick="login()">ログイン</button>
        </div>
        <nav class="navigation">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="mypage.php">MyPage</a></li>
            <li><a href="ranking.php">Ranking</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
        </nav>
      </header>
    </div>

    <!-- ログインチェック -->
    <?php
    // 入力されていない場合
    if (!(isset($_POST['username']) && isset($_POST['password']))) {
      header('Location: login.php');
      exit;
    } elseif ((strlen($_POST['username'])>30)||(strlen($_POST['password'])>10)) {
      header('Location: login.php');
      exit;
    }

    try {
      $db = new PDO('mysql:dbname=hew2020_00820;host=localhost;charset=utf8','hew2020_00820','');
    } catch (PDOException $e) {
      echo 'DB接続エラー ： ' . $e->getMessage();
    }

    $record = $db -> prepare('SELECT * FROM users WHERE username=?');
    $record -> bindParam(1, $_POST['username']);
    $record -> execute();
    $val = $record -> fetch();
    if (password_verify($_POST['password'], $val['password'])){
      ?>
      <script>
      $(function () {
        $('#done').fadeIn();
      });
      </script><?php
      // セッションに保存
      $_SESSION['username'] = $val['username'];
    } else {
      ?>
      <script>
      $(function() {
        $('#error').fadeIn();
      });
      </script>
      <?php
    }


    ?>

    <!-- ログイン成功 -->
    <div class="content_wrapper login_page" id="done" style="height:300px;display:none;">
      <h2>ログイン完了</h2>
      <p style="padding-top:10px;">ログインが完了しました。</p>
      <p style="padding-bottom:20px;">引き続きお楽しみください。</p>
      <button type="button" name="button"><a href="mypage.php" style="color:white;">MyPageへ</a></button>
    </div>


    <!-- ログイン失敗 -->
    <div class="content_wrapper login_page" id="error" style="height:300px;display:none;">
      <h2>ログイン失敗</h2>
      <p style="padding-top:10px;">ログインに失敗しました。</p>
      <p style="padding-bottom:20px;">ユーザー名・パスワードを確認してください。</p>
      <button type="button" name="button"><a href="login.php" style="color:white;">ログインページへ</a></button>
    </div>

    <script type="text/javascript" src="js/script.js"></script>

    <footer class="footer">
      <div class="social">
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
        <a href="#"><i class="fab fa-youtube"></i></a>
      </div>
      <small>&copy;Complete</small>
    </footer>
  </body>
</html>
