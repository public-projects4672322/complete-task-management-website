<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="none,noindex,nofollow">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/login.css">
    <link href="https://fonts.googleapis.com/css?family=Vollkorn"  rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Complete</title>
  </head>
  <body>
    <div class="header_wrapper">
      <header class="header">
        <div class="logo">
          <a href="index.php"><img src="images/logo.png" alt="Logo"></a>
        </div>
        <div class="login_header">
          <button type="button" name="button" onclick="logout()">ログアウト</button>
          <button type="button" name="button" onclick="login()">ログイン</button>
        </div>
        <nav class="navigation">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="mypage.php">MyPage</a></li>
            <li><a href="ranking.php">Ranking</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
        </nav>
      </header>
    </div>


    <!-- 登録確認チェック -->
    <?php
    // 入力されていない場合
    if (!(isset($_POST['username']) && isset($_POST['password']))) {
      header('Location: login.php');
      exit;
    } elseif ((strlen($_POST['username'])>30)||(strlen($_POST['password'])>10)) {
      header('Location: login.php');
      exit;
    }
    // 性別・誕生日は入力されていれば登録
    if (isset($_POST['gender'])) {
      $gender = $_POST['gender'];
    } else {
      $gender = "";
    }
    if (isset($_POST['year'])&&isset($_POST['month'])&&isset($_POST['date'])){
      $birthday = $_POST['year'].'-'.$_POST['month'].'-'.$_POST['date'];
    } else {
      $birthday = "";
    }

    try {
      $db = new PDO('mysql:dbname=hew2020_00820;host=localhost;charset=utf8','hew2020_00820','');
    } catch (PDOException $e) {
      echo 'DB接続エラー ： ' . $e->getMessage();
    }

    // ユーザー名が重複していないか確認
    $record = $db -> prepare('SELECT COUNT(*) AS num FROM users WHERE username LIKE ?');
    $record -> bindParam(1, $_POST['username']);
    $record -> execute();
    $num = $record -> fetch();


    if ($num['num']>0) {
      // ユーザ名が重複している場合
      ?>
      <script>
      $(function () {
        $('#multiple').fadeIn();
      });
      </script>
      <?php
    } else {
      // ユーザ名が登録していなければ
      // usersテーブルに挿入する
      $record = $db -> prepare('INSERT INTO users (username, password, gender, birthday, level) value(?,?,?,?,1)');

      $record -> bindParam(1, $_POST['username']);
      $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
      $record -> bindParam(2, $password);
      $record -> bindParam(3, $gender);
      $record -> bindParam(4, $birthday);

      $flag = $record -> execute();

      if ($flag) {
        ?>
        <script>
        $(function () {
          $('#done').fadeIn();
        });
        </script>
        <?php
        $_SESSION['username'] = $_POST['username'];
      } else {
        ?>
        <script>
        $(function () {
          $('#error').fadeIn();
        });
        </script>
        <?php
      }
    }
    ?>



    <!-- 登録が完了した場合 -->
    <div id="done" class="content_wrapper sign_up_page" style="display: none;">
      <h2>会員登録完了</h2>
      <p>会員登録が完了しました。</p>
      <table border="1" bordercolor="#c0c0c0" bgcolor="#f0f8ff">
        <tr>
          <th colspan="2" bgcolor="#87cefa">登録情報</th>
        </tr>
        <tr>
          <th class="title">ユーザー名</th>
          <th><?php print $_POST['username']; ?></th>
        </tr>
        <tr>
          <th class="title">性別</th>
          <th>
            <?php
            if($gender == 'man') {
              print "男性";
            } elseif ($gender == 'woman'){
              print "女性";
            } else {
              print "その他";
            }
            ?>
          </th>
        </tr>
        <tr>
          <th class="title">誕生日</th>
          <th><?php print $birthday; ?></th>
        </tr>
      </table>
      <p class="to_mypage"><a href="mypage.php">MyPageへ</a></p>
    </div>

    <!-- 登録できなかった時 （エラー）-->
    <div class="content_wrapper sign_up_page" id="multiple" style="height:300px;display:none;">
      <h2>登録未完了</h2>
      <p style="padding-top:10px;">ユーザ名がすでに使用されています。</p>
      <p style="padding-bottom:20px;">ユーザ名を変更してお試しください。</p>
      <button type="button" name="button"><a href="login.php" style="color:white;">会員登録画面へ</a></button>
    </div>

    <!-- 登録できなかった時 （エラー）-->
    <div class="content_wrapper sign_up_page" id="error" style="height:300px;display:none;">
      <h2>登録未完了</h2>
      <p style="padding-top:10px;">登録に失敗しました。</p>
      <p style="padding-bottom:20px;">申し訳ありませんが、再度お試しください。</p>
      <button type="button" name="button"><a href="login.php" style="color:white;">会員登録画面へ</a></button>
    </div>

    <script type="text/javascript" src="js/script.js"></script>

    <footer class="footer">
      <div class="social">
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
        <a href="#"><i class="fab fa-youtube"></i></a>
      </div>
      <small>&copy;Complete</small>
    </footer>
  </body>
</html>
