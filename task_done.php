<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="none,noindex,nofollow">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/task.css">
    <link href="https://fonts.googleapis.com/css?family=Vollkorn"  rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Complete</title>
  </head>
  <body>
    <div class="header_wrapper">
      <header class="header">
        <div class="logo">
          <a href="index.php"><img src="images/logo.png" alt="Logo"></a>
        </div>
        <div class="login_header">
          <button type="button" name="button" onclick="logout()">ログアウト</button>
          <button type="button" name="button" onclick="login()">ログイン</button>
        </div>
        <nav class="navigation">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="mypage.php">MyPage</a></li>
            <li><a href="ranking.php">Ranking</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
        </nav>
      </header>
    </div>

    <!-- タスク登録 -->
    <?php
    // form から内容を受け取る
    // タスク名が未入力であるとき戻る
    if (empty($_POST['task'])) {
      header("Location: task.php");
      exit;
    } else {
      $task = $_POST['task'];
    }
    $taskdate = date('Y').$_POST['task_month'].$_POST['task_date'];
    $starttime = $_POST['start_hour'].$_POST['start_min']."00";
    $endtime = $_POST['end_hour'].$_POST['end_min']."00";
    $memo = $_POST['memo'];
    $image = $_POST['image'];

    // データベース操作
    try {
      $db = new PDO('mysql:dbname=hew2020_00820;host=localhost;charset=utf8','hew2020_00820','');
    } catch (PDOException $e) {
      echo 'DB接続エラー ： ' . $e->getMessage();
    }
    // ユーザのIDを求める
    $userid = $db -> prepare('SELECT id FROM users WHERE username = ?');
    $userid -> bindParam(1, $_SESSION['username']);
    $userid -> execute();
    $id = $userid -> fetch();

    // INSERT
    $record = $db -> prepare('INSERT INTO tasks (id, created_at, taskdate, starttime, endtime, task, memo, image) VALUES(?,?,?,?,?,?,?,?)');


    // INSERTのVALUEに値を代入
    $record -> bindParam(1, $id['id']);
    $datetime = date('Y-m-d H:i:s');
    $record -> bindParam(2, $datetime);
    $record -> bindParam(3, $taskdate);
    $record -> bindParam(4, $starttime);
    $record -> bindParam(5, $endtime);
    $record -> bindParam(6, $task);
    $record -> bindParam(7, $memo);
    $record -> bindParam(8, $image);
    // 実行
    $flag = $record -> execute();

    // 問題なく実行されたかどうか
    if ($flag) {
      ?>
      <script>
        $(function (){
          $('#done').fadeIn();
        });
      </script>
      <?php
    } else {

    }

    ?>


    <div id="done" class="task_content" style="display: none;">
      <h2>タスク登録完了</h2>
      <p>タスクの登録が完了しました。</p>
      <p>しっかり達成できるように頑張りましょう。</p>
      <button type="button" name="button"><a href="mypage.php">MyPageへ</a></button>
    </div>

    <div id="done" class="task_content" style="display: none;">
      <h2>タスク未完了</h2>
      <p>タスクの登録ができませんでした。</p>
      <p>申し訳ありませんが、再度タスクを登録してください。</p>
      <button type="button" name="button"><a href="task.php">タスク登録へ</a></button>
    </div>


    <script type="text/javascript" src="js/script.js"></script>


    <footer class="footer">
      <div class="social">
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
        <a href="#"><i class="fab fa-youtube"></i></a>
      </div>
      <small>&copy;Complete</small>
    </footer>
  </body>
</html>
