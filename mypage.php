<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="none,noindex,nofollow">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/mypage.css">
    <link href="https://fonts.googleapis.com/css?family=Vollkorn"  rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
    <title>MY PAGE</title>
  </head>
  <body>
    <div class="header_wrapper">
      <header class="header">
        <div class="logo">
          <a href="index.php"><img src="images/logo.png" alt="Logo"></a>
        </div>
        <div class="login_header">
          <button type="button" name="button" onclick="logout()">ログアウト</button>
          <button type="button" name="button" onclick="login()">ログイン</button>
        </div>
        <nav class="navigation">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="mypage.php">MyPage</a></li>
            <li><a href="ranking.php">Ranking</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
        </nav>
      </header>
    </div>


    <!-- ユーザ進捗度 -->
    <div class="user_wrapper">
      <div class="content">
        <div class="left_content">
          <h3 class="username">
            <?php
            // ログインしている場合
            if (isset($_SESSION['username'])) {
              print $_SESSION['username'] . " さん";
              $flag = 1;

              // データベース操作
              try {
                $db = new PDO('mysql:dbname=hew2020_00820;host=localhost;charset=utf8','hew2020_00820','');
              } catch (PDOException $e) {
                echo 'DB接続エラー ： ' . $e->getMessage();
              }

              // レベルを取得
              $record = $db -> prepare('SELECT level FROM users WHERE username=?');
              $record -> bindParam(1, $_SESSION['username']);
              $record -> execute();
              $val = $record -> fetch();
              $lvl = $val['level'];
              $rank = floor($lvl/3);
              $required_lvl = 3 - ($lvl%3);

              // ユーザのIDを求める
              $userid = $db -> prepare('SELECT id FROM users WHERE username = ?');
              $userid -> bindParam(1, $_SESSION['username']);
              $userid -> execute();
              $id = $userid -> fetch();

              // 登録しているタスクが存在するか
              $exists = $db -> prepare('SELECT COUNT(*) as num FROM tasks WHERE id = ? AND taskdate = ?');
              $exists -> bindParam(1, $id['id']);
              $today = date("Y-m-d");
              $exists -> bindParam(2, $today);
              $exists -> execute();
              $exist = $exists -> fetch();

              // 登録しているタスクが存在するか
              $exists = $db -> prepare('SELECT COUNT(*) as num FROM tasks WHERE id = ?');
              $exists -> bindParam(1, $id['id']);
              $exists -> execute();
              $val = $exists -> fetch();

              // 達成率を調べる
              $cnt_achieve = $db -> prepare('SELECT COUNT(*) as num_achieve FROM tasks WHERE id = ? AND achieve = 1');
              $cnt_achieve -> bindParam(1, $id['id']);
              $cnt_achieve -> execute();
              $sum_achieve = $cnt_achieve -> fetch();

              if ($val['num']==0){
                $achievement = 0;
              } else {
                $calc = $sum_achieve['num_achieve'] / $val['num'] *100;
                $achievement = ceil($calc);
              }

            }// ログインしていない場合
            else {
              print "ゲスト さん";
              $lvl = 0;
              $rank = 0;
              $flag = 0;
              $val['num'] = 0;
              $required_lvl = 0;
              $achievement = 0;
            }
            ?>

          </h3>
          <p class="achievement">いままでの達成率は<?php print $achievement; ?>％です！</p>
        </div>
        <div class="right_content">
          <?php
          $icon = [
            ["ant","アリ"],
            [ "mouse","ねずみ"],
            [ "rabbit","うさぎ"],
            ["dog","いぬ"],
            [ "cat","ねこ"],
            [ "owl","ふくろう"],
            [ "eagle","わし"],
            [ "elephant","ぞう"],
            [ "cheetah","チーター"]
            ];
          $image = [
            'none' => '<i class="far fa-clipboard"></i>',
            'book' => '<i class="fas fa-book"></i>',
            'running' => '<i class="fas fa-running"></i>',
            'bed' => '<i class="fas fa-bed"></i>',
            'pc' => '<i class="fas fa-desktop"></i>',
            'meal' => '<i class="fas fa-utensils"></i>',
            'game' => '<i class="fas fa-gamepad"></i>',
            'building' => '<i class="fas fa-building"></i>'
          ];


          ?>
          <p class="show_level"><?php print "ランク：".$icon[$rank][1]."<br>レベル： ".$lvl."<br><span>あと{$required_lvl}回達成でランクアップ</span>"; ?></p>
          <img src="<?php print "images/{$icon[$rank][0]}.png" ?>" alt="user_icon">
        </div>
      </div>
    </div>





    <!-- 現在時刻表示 -->
    <p class="clock" id="realtime"></p>




    <!-- 本日のタスク -->
    <div class="task_header content_wrapper">
      <h2>本日のタスク</h2>
    </div>
    <!-- スケジュールスライド -->
    <div class="swiper-parent">
      <div class="swiper-container">
        <div class="swiper-wrapper" id="taskSlide">
          <?php
          if ($flag == 1) {
            // 登録している情報を取り出す
            $records = $db -> prepare('SELECT * FROM tasks WHERE id = ? AND taskdate = ? ORDER BY starttime');
            $records -> bindParam(1, $id['id']);
            $today = date('Y-m-d');
            $records -> bindParam(2, $today);
            $records -> execute();
          }
          // ログインしていない or 今日のタスクがない
          if ($flag == 0 || $exist['num'] == 0) {
            $slide_view = 1;
            ?>
            <div class="swiper-slide">
              <div class="slide-content">
                <p class="name">タスク未登録</p>
              </div>
            </div>
            <?php
          } elseif ($exist['num'] == 1) {
            $slide_view = 1;
            ?>
            <div class="swiper-slide">
              <div class="slide-content">
                <?php
                while($record = $records -> fetch()) {
                  print "<p class='name'>{$record['task']}</p>";
                  print "<p class='image'>{$image[$record['image']]}</p>";
                  $temp1 = explode(":",$record['starttime']);
                  $start = $temp1[0].":".$temp1[1];
                  $temp2 = explode(":",$record['endtime']);
                  $end = $temp2[0].":".$temp2[1];
                  print "<p class='time'>{$start}～{$end}</p>";
                  print "<p class='memo'>{$record['memo']}</p>";
                  // id用に加工
                  $temp = explode(" ", $record['created_at']);
                  $date = explode("-", $temp[0]);
                  $time = explode(":", $temp[1]);
                  $datetime = $date[0].$date[1].$date[2].$time[0].$time[1].$time[2];
                  // 達成しているかチェック
                  if ($record['achieve']) {
                    print "<form method='post' action='task_achieve.php'>";
                    print "<input type='hidden' name='created_at' value='{$datetime}'>";
                    print "<input type='hidden' name='flag' value='1'>";
                    print "<input type='hidden' name='id' value='{$id['id']}'>";
                    print "<input type='submit' value='達成完了!'>";
                    print "</form>";
                  } else {
                    print "<form method='post' action='task_achieve.php'>";
                    print "<input type='hidden' name='created_at' value='{$datetime}'>";
                    print "<input type='hidden' name='flag' value='0'>";
                    print "<input type='hidden' name='id' value='{$id['id']}'>";
                    print "<input type='submit' value='未達成'>";
                    print "</form>";
                  }
                }
                ?>
              </div>
            </div>
            <?php
          } elseif ($exist['num'] == 2) {
            $slide_view = 2;
            while($record = $records -> fetch()) {
              ?>
              <div class="swiper-slide">
              <div class="slide-content">
              <?php
              print "<p class='name'>{$record['task']}</p>";
              print "<p class='image'>{$image[$record['image']]}</p>";
              $temp1 = explode(":",$record['starttime']);
              $start = $temp1[0].":".$temp1[1];
              $temp2 = explode(":",$record['endtime']);
              $end = $temp2[0].":".$temp2[1];
              print "<p class='time'>{$start}～{$end}</p>";
              print "<p class='memo'>{$record['memo']}</p>";
              // id用に加工
              $temp = explode(" ", $record['created_at']);
              $date = explode("-", $temp[0]);
              $time = explode(":", $temp[1]);
              $datetime = $date[0].$date[1].$date[2].$time[0].$time[1].$time[2];
              // 達成しているかチェック
              if ($record['achieve']) {
                print "<form method='post' action='task_achieve.php'>";
                print "<input type='hidden' name='created_at' value='{$datetime}'>";
                print "<input type='hidden' name='flag' value='1'>";
                print "<input type='hidden' name='id' value='{$id['id']}'>";
                print "<input type='submit' value='達成完了!'>";
                print "</form>";
              } else {
                print "<form method='post' action='task_achieve.php'>";
                print "<input type='hidden' name='created_at' value='{$datetime}'>";
                print "<input type='hidden' name='flag' value='0'>";
                print "<input type='hidden' name='id' value='{$id['id']}'>";
                print "<input type='submit' value='未達成'>";
                print "</form>";
              }
              ?>
              </div>
              </div>
              <?php
            }
          } else {
            $slide_view = 3;
            while($record = $records -> fetch()) {
              ?>
              <div class="swiper-slide">
              <div class="slide-content">
              <?php
              print "<p class='name'>{$record['task']}</p>";
              print "<p class='image'>{$image[$record['image']]}</p>";
              $temp1 = explode(":",$record['starttime']);
              $start = $temp1[0].":".$temp1[1];
              $temp2 = explode(":",$record['endtime']);
              $end = $temp2[0].":".$temp2[1];
              print "<p class='time'>{$start}～{$end}</p>";
              print "<p class='memo'>{$record['memo']}</p>";
              // id用に加工
              $temp = explode(" ", $record['created_at']);
              $date = explode("-", $temp[0]);
              $time = explode(":", $temp[1]);
              $datetime = $date[0].$date[1].$date[2].$time[0].$time[1].$time[2];
              // 達成しているかチェック
              if ($record['achieve']) {
                print "<form method='post' action='task_achieve.php'>";
                print "<input type='hidden' name='created_at' value='{$datetime}'>";
                print "<input type='hidden' name='flag' value='1'>";
                print "<input type='hidden' name='id' value='{$id['id']}'>";
                print "<input type='submit' value='達成完了!'>";
                print "</form>";
              } else {
                print "<form method='post' action='task_achieve.php'>";
                print "<input type='hidden' name='created_at' value='{$datetime}'>";
                print "<input type='hidden' name='flag' value='0'>";
                print "<input type='hidden' name='id' value='{$id['id']}'>";
                print "<input type='submit' value='未達成'>";
                print "</form>";
              }
              ?>
              </div>
              </div>
              <?php
            }
          }
          ?>

        </div>
      	<div class="swiper-pagination"></div>
      </div>
      <div class="swiper-button-prev"></div>
      <div class="swiper-button-next"></div>
    </div>

    <!-- タスク登録ボタン -->
    <div class="add_task">
      <button id="addBtn" type="button" name="button" onclick="loginAlert"><a id="addBtn_a" href="task.php" disabled><i class="fas fa-plus" style="font-size: 20px;"></i>  タスク登録</a></button>
    </div>


    <!-- スケジュール一覧 -->
    <div class="task_all content_wrapper">
      <h2>直近タスク一覧</h2>
      <div class="flex_wrapper">
        <?php
        if ($val['num'] > 0) {
          // 登録している情報を取り出す
          $records = $db -> prepare('SELECT * FROM tasks WHERE id = ? ORDER BY taskdate DESC LIMIT 9');
          $records -> bindParam(1, $id['id']);
          $records -> execute();
          // 存在する場合、タスクを取り出す
          while($record = $records -> fetch()){
            // id用に加工
            $temp1 = explode(" ", $record['created_at']);
            $date1 = explode("-", $temp1[0]);
            $time1 = explode(":", $temp1[1]);
            $datetime1 = $date1[0].$date1[1].$date1[2].$time1[0].$time1[1].$time1[2];
            print '<div class="flex_inner">';
            print "<p class='name'>{$record['task']}</p>";
            print "<p class='taskdate'>{$record['taskdate']}</p>";
            $temp1 = explode(":",$record['starttime']);
            $start = $temp1[0].":".$temp1[1];
            $temp2 = explode(":",$record['endtime']);
            $end = $temp2[0].":".$temp2[1];
            print "<p class='time'>{$start}～{$end}</p>";
            print "<p class='memo'>{$record['memo']}</p>";
            print "<form method='post' action='task_achieve.php'>";
            print "<input type='hidden' name='created_at' value='{$datetime1}'>";
            print "<input type='hidden' name='id' value='{$id['id']}'>";
            print "<input type='submit' name='delete' value='削除する'>";
            print "</form>";
            print '</div>';
          }
        } else {
          ?>
          <div class="flex_inner">
            <p class="name">未登録</p>
            <p>登録してあるタスクがありません。タスクを登録してみましょう。</p>
          </div>
          <?php
        }

        ?>
      </div>
    </div>



    <footer class="footer">
      <div class="social">
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
        <a href="#"><i class="fab fa-youtube"></i></a>
      </div>
      <small>&copy;Complete</small>
    </footer>


    <script type="text/javascript" src="js/script.js"></script>
    <!-- Swiper スクリプト -->
    <script type="text/javascript" src="js/mypage.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>
    <script>
      var mySwiper = new Swiper('.swiper-container', {
        centeredSlides: true,
        slidesPerView: <?php print $slide_view; ?>,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev'
        },
        pagination: {
          el: '.swiper-pagination',
          type: 'bullets',
          clickable: true
        }
      });

      // ログインしていないときはボタンを非活性に
      window.addEventListener('load', () => {
        var flag = <?php print $flag; ?>;
        if (flag == 0){
          document.getElementById('addBtn_a').href = 'javascript:void(0)';
          document.getElementById('addBtn_a').innerHTML = "ログインすることで追加できます";
        }
      });




    </script>
  </body>
</html>
