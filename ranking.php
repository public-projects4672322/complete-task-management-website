<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="none,noindex,nofollow">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/ranking.css">
    <link href="https://fonts.googleapis.com/css?family=Vollkorn"  rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <title>Ranking</title>
  </head>
  <body>
    <div class="header_wrapper">
      <header class="header">
        <div class="logo">
          <a href="index.php"><img src="images/logo.png" alt="Logo"></a>
        </div>
        <div class="login_header">
          <button type="button" name="button" onclick="logout()">ログアウト</button>
          <button type="button" name="button" onclick="login()">ログイン</button>
        </div>
        <nav class="navigation">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="mypage.php">MyPage</a></li>
            <li><a href="ranking.php">Ranking</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
        </nav>
      </header>
    </div>

    <?php
    // データベース操作
    try {
      $db = new PDO('mysql:dbname=hew2020_00820;host=localhost;charset=utf8','hew2020_00820','');
    } catch (PDOException $e) {
      echo 'DB接続エラー ： ' . $e->getMessage();
    }

    print '<div class="ranking content_wrapper">';
    print '<h2>ランキング</h2>';
    $i = 1;
    $records = $db -> query('SELECT * FROM users ORDER BY level DESC LIMIT 3');
    while ($record = $records -> fetch()){
      print '<div class="ranking_record">';
      print '<dl>';
      print '<dt class="num">'.$i.'</dt>';
      print '<dd class="name">'.$record['username'].' さん</dd>';
      print '<dd class="lvl">レベル'.$record['level'].'</dd>';
      print '</dl>';
      print '</div>';
      $i++;
    }
    print '</div>';
    ?>

    <footer class="footer">
      <div class="social">
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
        <a href="#"><i class="fab fa-youtube"></i></a>
      </div>
      <small>&copy;Complete</small>
    </footer>
    <script type="text/javascript" src="js/script.js"></script>
  </body>
</html>
