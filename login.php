<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="none,noindex,nofollow">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/login.css">
    <link href="https://fonts.googleapis.com/css?family=Vollkorn"  rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Complete</title>
  </head>
  <body onload="setDate()">
    <div class="header_wrapper">
      <header class="header">
        <div class="logo">
          <a href="index.php"><img src="images/logo.png" alt="Logo"></a>
        </div>
        <div class="login_header">
          <button type="button" name="button" onclick="logout()">ログアウト</button>
          <button type="button" name="button" onclick="login()">ログイン</button>
        </div>
        <nav class="navigation">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="mypage.php">MyPage</a></li>
            <li><a href="ranking.php">Ranking</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
        </nav>
      </header>
    </div>


    <!-- 会員登録 ログイン選択 -->
    <div id="selectPage" class="content_wrapper login_select">
      <h2>ログイン</h2>
      <dl>
        <dt>アカウントをお持ちの方</dt>
        <dd><button id="loginBtn" class="login_btn" type="button">ログインする</button></dd>
      </dl>
      <dl>
        <dt>アカウントをお持ちでない方</dt>
        <dd><button id="signUpBtn" class="signup_btn" type="button">会員登録する</button></dd>
      </dl>
    </div>

    <!--*********************************
          ログイン
      *********************************-->
    <div id="loginPage" class="content_wrapper"  style="display: none;">
      <div class="login_page">
        <h2>ログイン</h2>
        <form action="login_check.php" method="post">
          <dl>
            <dt>ユーザ名<span>※</span><br><small>10文字以内</small></dt>
            <dd><input type="text" name="username" placeholder="Username" required maxlength="10"></dd>
          </dl>
          <dl>
            <dt>パスワード<span>※</span><br><small>10文字以内</small></dt>
            <dd><input type="password" name="password" placeholder="Password" required maxlength="10"></dd>
          </dl>
          <dl>
            <input type="submit" value="ログイン">
          </dl>
        </form>
      </div>
    </div>

    <!-- 会員登録 -->
    <div id="signUpPage" class="sign_up content_wrapper" style="display: none;">
      <div class="sign_up_page">
        <h2>会員登録</h2>
        <form action="signup_check.php" method="post">
          <dl>
            <dt>ユーザー名<span>※</span><br><small>10文字以内</small></dt>
            <dd><input type="text" name="username" placeholder="Username" required maxlength="10"></dd>
          </dl>
          <dl>
            <dt>パスワード<span>※</span><br><small>10文字以内</small></dt>
            <dd><input type="text" name="password" placeholder="Password" required maxlength="10"></dd>
          </dl>
          <dl>
            <dt>性別<span>※</span></dt>
            <dd>
              <input id="man" type="radio" name="gender" value="man" checked><label for="man">男性</label>
              <input id="woman" type="radio" name="gender" value="woman"><label for="woman">女性</label>
              <input id="others" type="radio" name="gender" value="others"><label for="others">その他</label>
            </dd>
          </dl>
          <dl>
            <dt>生年月日<span>※</span></dt>
            <dd>
              <!-- 年選択 -->
              <select id="year" name="year">
                <?php
                for ($i=date('Y')-120; $i<=date('Y'); $i++) {
                  if ($i == 2000){
                    print '<option value="'.$i.'" selected>'.$i.'</option>';
                  } else {
                    print '<option value="'.$i.'">'.$i.'</option>';
                  }
                }
                ?>
              </select>年
              <!-- 月選択 -->
              <select id="month" name="month">
                <?php
                for ($i=1; $i<=12; $i++) {
                  print '<option value="'.$i.'">'.$i.'</option>';
                }
                ?>
              </select>月
              <!-- 日付選択 -->
              <select id="date" name="date" onclick="setDate()">
              </select>日
            </dd>
          </dl>
          <dl>
            <input type="submit" value="上記の内容で登録">
          </dl>
        </form>
      </div>
    </div>





    <!-- javascript -->
    <script type="text/javascript" src="js/script.js"></script>
    <script>
    // ログイン or 会員登録 選択
    $(function() {
    $('#loginBtn').on('click', () => {
         $.when(
           $('#selectPage').fadeOut()
         ).done( () => {
　　　　　　$('#loginPage').fadeIn();
          });
       });
    });

    $(function() {
    $('#signUpBtn').on('click', () => {
         $.when(
           $('#selectPage').fadeOut()
         ).done( () => {
　　　　　　$('#signUpPage').fadeIn();
          });
       });
    });

    // 日付の日数を動的に変化
    function setDate() {
      const year = document.getElementById('year').value;
      const month = document.getElementById('month').value;
      var d = new Date();
      var lastDate = new Date(year, month, 0);
      let date = document.getElementById('date');
      document.createElement('option');
      for (let i = 1; i <= lastDate.getDate(); i++){
        let option = document.createElement('option');
        option.setAttribute('value', i);
        option.innerHTML = i;
        date.appendChild(option);
      }
    }


    </script>



    <footer class="footer">
      <div class="social">
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
        <a href="#"><i class="fab fa-youtube"></i></a>
      </div>
      <small>&copy;Complete</small>
    </footer>
  </body>
</html>
