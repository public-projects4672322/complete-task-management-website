<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="none,noindex,nofollow">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/contact.css">
    <link href="https://fonts.googleapis.com/css?family=Vollkorn"  rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <title>Complete</title>
  </head>
  <body>
    <div class="header_wrapper">
      <header class="header">
        <div class="logo">
          <a href="index.php"><img src="images/logo.png" alt="Logo"></a>
        </div>
        <div class="login_header">
          <button type="button" name="button" onclick="logout()">ログアウト</button>
          <button type="button" name="button" onclick="login()">ログイン</button>
        </div>
        <nav class="navigation">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="mypage.php">MyPage</a></li>
            <li><a href="ranking.php">Ranking</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
        </nav>
      </header>
    </div>


    <div class="submit_confirm">
      <h2>送信完了</h2>
      <dl>
        <dt>メール</dt>
        <dd><?php print $_COOKIE['save_email']; ?></dd>
      </dl>
      <dl>
        <dt>件名</dt>
        <dd><?php print $_COOKIE['save_subject']; ?></dd>
      </dl>
      <dl>
        <dt>本文</dt>
        <dd><?php print $_COOKIE['save_maintext']; ?></dd>
      </dl>
    </div>

    <!-- Cookie削除 -->
    <?php
    setcookie('save_email', '', time() - 30);
    setcookie('save_subject', '', time() - 30);
    setcookie('save_maintext', '', time() - 30);
    ?>

    <script type="text/javascript" src="js/script.js"></script>

    <footer class="footer">
      <div class="social">
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
        <a href="#"><i class="fab fa-youtube"></i></a>
      </div>
      <small>&copy;Complete</small>
    </footer>
  </body>
</html>
