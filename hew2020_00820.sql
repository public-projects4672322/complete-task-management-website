-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 2021-02-27 21:42:05
-- サーバのバージョン： 10.4.11-MariaDB
-- PHP のバージョン: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `hew2020_00820`
--
CREATE DATABASE IF NOT EXISTS `hew2020_00820` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `hew2020_00820`;

-- --------------------------------------------------------

--
-- テーブルの構造 `tasks`
--

DROP TABLE IF EXISTS `tasks`;
CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `taskdate` date NOT NULL,
  `starttime` time NOT NULL,
  `endtime` time NOT NULL,
  `task` varchar(100) NOT NULL,
  `memo` varchar(100) DEFAULT NULL,
  `image` varchar(10) NOT NULL,
  `achieve` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- テーブルのデータのダンプ `tasks`
--

INSERT INTO `tasks` (`id`, `created_at`, `taskdate`, `starttime`, `endtime`, `task`, `memo`, `image`, `achieve`) VALUES
(2, '2021-02-26 14:43:18', '2021-01-01', '12:00:00', '12:00:00', 'ads', 'ad', 'none', 0),
(2, '2021-02-26 14:43:28', '2021-01-01', '12:00:00', '12:00:00', 'afs', '', 'none', 0),
(2, '2021-02-26 14:44:40', '2021-04-01', '12:00:00', '12:00:00', 'test', '', 'none', 0),
(2, '2021-02-26 16:04:29', '2021-01-01', '01:00:00', '12:00:00', 'あお', 'asdfsafsaf', 'none', 0),
(3, '2021-02-26 23:09:56', '2021-01-01', '12:00:00', '12:00:00', 'test', 'test', 'none', 0),
(3, '2021-02-26 23:50:28', '2021-01-01', '12:00:00', '12:00:00', 'tetetet', 'tet', 'running', 0),
(3, '2021-02-26 23:55:34', '2021-01-01', '12:00:00', '12:00:00', 'sdafdsafa', 'dsfasfsad', 'book', 0),
(3, '2021-02-27 00:11:51', '2021-02-27', '12:00:00', '12:00:00', 'おはよう', '', 'bed', 1),
(3, '2021-02-27 00:12:02', '2021-01-01', '12:00:00', '12:00:00', 'fgsdgsdfg', 'fsdgsfdg', 'meal', 0),
(3, '2021-02-27 00:12:35', '2021-02-27', '03:00:00', '12:00:00', 'PC', '', 'pc', 1),
(3, '2021-02-27 00:12:50', '2021-02-27', '12:00:00', '12:00:00', '読書', '', 'book', 1),
(4, '2021-02-27 11:16:14', '2021-02-27', '12:00:00', '12:00:00', 'tetetetsss', '', 'pc', 1),
(4, '2021-02-27 13:35:30', '2021-02-27', '09:00:00', '12:00:00', 'daf', '', 'running', 0),
(4, '2021-02-27 13:38:25', '2021-02-27', '12:00:00', '12:00:00', 'adsfasf', '', 'running', 0),
(5, '2021-02-27 15:57:57', '2021-02-28', '05:00:00', '05:00:00', 'おはよう', 'しっかり起きよう', 'bed', NULL),
(5, '2021-02-27 15:58:33', '2021-02-27', '20:00:00', '21:00:00', 'おやすみ', 'しっかり寝よう', 'bed', 1),
(5, '2021-02-27 20:16:03', '2021-02-27', '20:00:00', '21:00:00', '運動', '', 'running', 1),
(5, '2021-02-27 20:45:11', '2021-02-27', '12:00:00', '12:00:00', '勉強', '', 'book', 1),
(7, '2021-02-27 20:58:17', '2021-02-27', '08:00:00', '09:00:00', '運動', '', 'running', 1),
(7, '2021-02-27 20:58:43', '2021-02-27', '10:00:00', '11:30:00', '勉強', '', 'book', 1),
(7, '2021-02-27 21:16:35', '2021-02-27', '12:00:00', '12:00:00', '昼食', '', 'meal', 1);

-- --------------------------------------------------------

--
-- テーブルの構造 `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `password` char(60) DEFAULT NULL,
  `gender` char(10) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

--
-- テーブルのデータのダンプ `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `gender`, `birthday`, `level`) VALUES
(1, 'test', '$2y$10$1oM0jWrlGg3NuUYCQLN4EeMLrK923n73aH/XFZmj.XZLMFjTEV2xm', 'man', '2000-01-01', 1),
(2, 'test1', '$2y$10$3KIWsZ8J0AORTz1JmKErouxoh3LpslLzJoFgun88M94mqv9BhDI1G', 'man', '2000-01-01', 2),
(3, 'test2', '$2y$10$g98RrwcOFC/KO.ABaPGzV.XbuBWFlEsZaI9E3c/oTlTta7WknZvhe', 'man', '2000-01-01', 3),
(4, '一位とるマン', '$2y$10$HUkpbaghm/tYRR/JiYrAUuuY3EED00PYsiug4OTp7YDpcomGTy8ta', 'man', '2000-01-01', 9),
(5, 'test4', '$2y$10$r1G/AkZfBxHcJ0bQUCIok.7MXodAYDblwm2t9koFMBJ3KFCX2rPgm', 'woman', '2001-06-01', 3),
(7, 'がんばるマン', '$2y$10$/cD4qFf4GuZpWwhte6aKTOlcLetBw25mYPqr1PbACrQzcR7wvVbfq', 'man', '2000-01-01', 3);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
