<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="none,noindex,nofollow">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/task.css">
    <link href="https://fonts.googleapis.com/css?family=Vollkorn"  rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <title>Complete</title>
  </head>
  <body>
    <div class="header_wrapper">
      <header class="header">
        <div class="logo">
          <a href="index.php"><img src="images/logo.png" alt="Logo"></a>
        </div>
        <div class="login_header">
          <button type="button" name="button" onclick="logout()">ログアウト</button>
          <button type="button" name="button" onclick="login()">ログイン</button>
        </div>
        <nav class="navigation">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="mypage.php">MyPage</a></li>
            <li><a href="ranking.php">Ranking</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
        </nav>
      </header>
    </div>

    <!-- タスク登録画面 -->
    <div class="task">
      <div class="task_content">
        <h2>タスク登録</h2>
        <form action="task_done.php" method="post">
          <dl>
            <dt>タスク名<small style="color:red;">※</small></dt>
            <dd><input type="text" name="task" required placeholder="タスク名" maxlength="10"></dd>
          </dl>
          <dl>
            <dt>日時</dt>
            <dd>
              <select name="task_month">
                <?php
                $this_month = date('m');
                for ($i=1; $i<=12; $i++) {
                  if ($i < 10){
                    $i = "0".$i;
                  }
                  if ($i == $this_month){
                    print "<option value='{$i}' selected>{$i}</option>";
                  }
                  print "<option value='{$i}'>{$i}</option>";
                }
                ?>
              </select>月
              <select name="task_date">
                <?php
                $today = date('d');
                for ($i=1; $i<=31; $i++) {
                  if ($i < 10){
                    $i = "0".$i;
                  }
                  if ($i == $today){
                    print "<option value='{$i}' selected>{$i}</option>";
                  }
                  print "<option value='{$i}'>{$i}</option>";
                }
                ?>
              </select>日
            </dd>
          </dl>
          <dl>
            <dt>開始時刻～終了時刻</dt>
            <dd>
              <select name="start_hour">
                <?php
                for ($i=0; $i<24; $i++){
                  if ($i<10) {
                    $i = '0'.$i;
                  }
                  if ($i==12) {
                    print "<option value='{$i}' selected>{$i}</option>";
                  } else {
                    print "<option value='{$i}'>{$i}</option>";
                  }
                }?></select>：<select name="start_min"><?php
                for ($i=0; $i<=60; $i++) {
                  if ($i<10) {
                    $i = '0'.$i;
                  }
                  print "<option value='{$i}'>{$i}</option>";
                }
                ?></select> ～ <select name="end_hour"><?php
                for ($i=0; $i<24; $i++){
                  if ($i<10) {
                    $i = '0'.$i;
                  }
                  if ($i==12) {
                    print "<option value='{$i}' selected>{$i}</option>";
                  } else {
                    print "<option value='{$i}'>{$i}</option>";
                  }
                }?></select>：<select name="end_min"><?php
                for ($i=0; $i<=60; $i++) {
                  if ($i<10) {
                    $i = '0'.$i;
                  }
                  print "<option value='{$i}'>{$i}</option>";
                }
                ?></select>
            </dd>
          </dl>
          <dl>
            <dt>一言メモ</dt>
            <dd> <input type="text" name="memo" placeholder="MEMO" maxlength="10"> </dd>
          </dl>
          <dl class="image_radio">
            <dt>イメージ選択</dt>
            <div class="flex-wrapper">
              <dd>
                <label for="book"><i class="fas fa-book"></i></label><br><input id="book" type="radio" name="image" value="book">
              </dd>
              <dd>
                <label for="building"><i class="fas fa-building"></i></label><br><input id="building" type="radio" name="image" value="building">
              </dd>
              <dd>
                <label for="running"><i class="fas fa-running"></i></label><br><input id="running" type="radio" name="image" value="running">
              </dd>
              <dd>
                <label for="pc"><i class="fas fa-desktop"></i></label><br><input id="pc" type="radio" name="image" value="pc">
              </dd>
              <dd>
                <label for="game"><i class="fas fa-gamepad"></i></label><br><input id="game" type="radio" name="image" value="game">
              </dd>
              <dd>
                <label for="meal"><i class="fas fa-utensils"></i></label><br><input id="meal" type="radio" name="image" value="meal">
              </dd>
              <dd>
                <label for="bed"><i class="fas fa-bed"></i></label><br><input id="bed" type="radio" name="image" value="bed">
              </dd>
              <dd>
                <label for="none">なし</label><br><input id="none" type="radio" name="image" value="none" checked>
              </dd>
            </div>
          </dl>
          <dl class="btn">
            <dd><input type="submit" value="登録"></dd>
          </dl>
        </form>
      </div>
    </div>

    <script type="text/javascript" src="js/script.js"></script>

    <footer class="footer">
      <div class="social">
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
        <a href="#"><i class="fab fa-youtube"></i></a>
      </div>
      <small>&copy;Complete</small>
    </footer>
  </body>
</html>
