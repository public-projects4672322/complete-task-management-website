<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="none,noindex,nofollow">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/contact.css">
    <link href="https://fonts.googleapis.com/css?family=Vollkorn"  rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <title>Contact</title>
  </head>
  <body>
    <div class="header_wrapper">
      <header class="header">
        <div class="logo">
          <a href="index.php"><img src="images/logo.png" alt="Logo"></a>
        </div>
        <div class="login_header">
          <button type="button" name="button" onclick="logout()">ログアウト</button>
          <button type="button" name="button" onclick="login()">ログイン</button>
        </div>
        <nav class="navigation">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="mypage.php">MyPage</a></li>
            <li><a href="ranking.php">Ranking</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
        </nav>
      </header>
    </div>


    <!-- FORM情報 ある場合 -->



    <!-- お問い合わせ -->
    <div class="form_wrapper content_wrapper">
      <h2>Contact</h2>
      <form class="form" action="submit.php" method="post">
        <dl>
          <dt>メール<span>※</span></dt>
          <dd><input id="email" type="text" name="email" placeholder="メールアドレスを入力してください" maxlength="50"
            value="<?php if (isset($_COOKIE['save_email'])){print $_COOKIE['save_email'];} ?>" required>
          </dd>
        </dl>
        <dl>
          <dt>件名<span>※</span></dt>
          <dd><input id="subject" type="text" name="subject" placeholder="件名を入力してください" maxlength="20"
            value="<?php if (isset($_COOKIE['save_subject'])){print $_COOKIE['save_subject'];}?>" required>
          </dd>
        </dl>
        <dl>
          <dt>本文<span>※</span></dt>
          <dd>
            <textarea name="maintext" placeholder="お問い合わせ内容をできるだけ詳しく入力してください" maxlength="1000" required><?php
            if (isset($_COOKIE['save_maintext'])) {
              print $_COOKIE['save_maintext'];
            }?></textarea>
          </dd>
        </dl>
        <div class="btn">
          <input class="submit_btn" type="submit" value="確認画面へ">
        </div>
      </form>
    </div>

    <script type="text/javascript" src="js/script.js"></script>

    <footer class="footer">
      <div class="social">
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
        <a href="#"><i class="fab fa-youtube"></i></a>
      </div>
      <small>&copyComplete</small>
    </footer>
  </body>
</html>
