<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="none,noindex,nofollow">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/contact.css">
    <link href="https://fonts.googleapis.com/css?family=Vollkorn"  rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <title>Complete</title>
  </head>
  <body>
    <div class="header_wrapper">
      <header class="header">
        <div class="logo">
          <a href="index.php"><img src="images/logo.png" alt="Logo"></a>
        </div>
        <div class="login_header">
          <button type="button" name="button" onclick="logout()">ログアウト</button>
          <button type="button" name="button" onclick="login()">ログイン</button>
        </div>
        <nav class="navigation">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="mypage.php">MyPage</a></li>
            <li><a href="ranking.php">Ranking</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
        </nav>
      </header>
    </div>

    <!-- FORM内容受け取り -->
    <?php
    $email = htmlspecialchars($_POST['email'], ENT_QUOTES);
    $subject = htmlspecialchars($_POST['subject'], ENT_QUOTES);
    $maintext =htmlspecialchars($_POST['maintext'], ENT_QUOTES);
    // Cookieに一時的に保存
    setcookie('save_email', $email, time() + 60 * 30);
    setcookie('save_subject', $subject, time() + 60 * 30);
    setcookie('save_maintext', $maintext, time() + 60 * 30);
    ?>

    <div class="submit_confirm">
      <h2>確認画面</h2>
      <dl>
        <dt>メール</dt>
        <dd><?php print $email ?></dd>
      </dl>
      <dl>
        <dt>件名</dt>
        <dd><?php print $subject ?></dd>
      </dl>
      <dl>
        <dt>本文</dt>
        <dd><?php print $maintext ?></dd>
      </dl>
      <div class="btn_group">
        <div class="btn">
          <form class="" action="contact.php" method="post">
            <input class="submit_btn" type="submit" value="もう一度入力">
          </form>
        </div>
        <div class="btn">
          <form class="" action="submit_done.php" method="post">
            <input class="submit_btn" type="submit" value="送信完了">
          </form>
        </div>
      </div>
    </div>

    <script type="text/javascript" src="js/script.js"></script>

    <footer class="footer">
      <div class="social">
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
        <a href="#"><i class="fab fa-youtube"></i></a>
      </div>
      <small>&copy;Complete</small>
    </footer>
  </body>
</html>
