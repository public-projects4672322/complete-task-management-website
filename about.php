<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="none,noindex,nofollow">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/about.css">
    <link href="https://fonts.googleapis.com/css?family=Vollkorn"  rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <title>Complete</title>
  </head>
  <body>
    <div class="header_wrapper">
      <header class="header">
        <div class="logo">
          <a href="index.php"><img src="images/logo.png" alt="Logo"></a>
        </div>
        <div class="login_header">
          <button type="button" name="button" onclick="logout()">ログアウト</button>
          <button type="button" name="button" onclick="login()">ログイン</button>
        </div>
        <nav class="navigation">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="mypage.php">MyPage</a></li>
            <li><a href="ranking.php">Ranking</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
        </nav>
      </header>
    </div>

    <!-- サイト紹介 -->
    <div class="site_introduce">
      <h2>サイト紹介</h2>
      <div class="content">
        <p>
          こんにちは。<br>
          「Complete」をご利用いただきありがとうございます。
        </p>
        <p>
          このWebサイトでは、タスク管理サービスを提供しています。タスク管理サービスを提供しているサービスは他にもたくさんあると思いますが、その中でもこのサイトではタスクを完了させるといったことに力を入れています。やるべきことを忘れてしまうといった方や決めたことをやりきる習慣をつけたい方などには特におすすめのサービスとなっています。
        </p>
        <p>
          使い方に関しては下記のチュートリアルをご覧ください。
        </p>
      </div>
    </div>

    <!-- できること -->
    <div class="introduction">
      <h2>チュートリアル</h2>
      <ul>
        <li class="main-list">1,会員登録 or ログイン</li>
        <p>まずは無料会員登録・会員の方はログインをしていただくことで十分なサービスを受けることができます。<br>
          個別でタスクを登録・管理していくので、ログインはほぼ必須となります。簡単に会員登録できるので早速<a href="login.php"><span class="under_blue">会員登録・ログイン</span></a>してみてください。</p>
        <li class="main-list">2,タスクを登録</li>
        <p>ログインしたあとはまずタスクを登録してみましょう。タスクの登録は<a href="mypage.php"><span class="under_blue">Mypageの「タスクを追加」</span></a>から可能です。</p>
        <p>タスクの登録も簡単に行うことができます。画面の指示にしたがって文字を入力又は選択することで登録することができます。まずは直近のやるべきことを追加してみてはいかがでしょうか。
        </p>
        <li class="main-list">3,タスクの達成度管理</li>
        <p>タスクを登録した後は、タスクの達成度管理をしてみましょう。タスクの達成度管理は<a href="mypage.php"><span class="under_blue">Mypage</span></a>から可能です。</p>
        <p>タスクを登録するだけでなく、それを管理し達成することでこのサイトでは「レベル」を上げることができます。そしてレベルが一定まで上がるとランクが上がります。ユーザのレベルを上げることで<a href="mypage.php"><span class="under_blue">ランキング</span></a>に名を連ねることもできます。<br>
        登録したタスクをしっかりこなすことでレベルは上がるので頑張ってみてください。</p>
      </ul>
    </div>

    <script type="text/javascript" src="js/script.js"></script>

    <footer class="footer">
      <div class="social">
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
        <a href="#"><i class="fab fa-youtube"></i></a>
      </div>
      <small>&copy;Complete</small>
    </footer>
  </body>
</html>
