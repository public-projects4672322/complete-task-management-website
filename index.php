<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="none,noindex,nofollow">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/jquery.skippr.css">
    <link href="https://fonts.googleapis.com/css?family=Vollkorn"  rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Complete</title>
  </head>
  <body>
    <!-- ヘッダー -->
    <div class="header_wrapper">
      <header class="header">
        <div class="logo">
          <a href="index.php"><img src="images/logo.png" alt="Logo"></a>
        </div>
        <div class="login_header">
          <button type="button" name="button" onclick="logout()">ログアウト</button>
          <button type="button" name="button" onclick="login()">ログイン</button>
        </div>
        <nav class="navigation">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="mypage.php">MyPage</a></li>
            <li><a href="ranking.php">Ranking</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
        </nav>
      </header>
    </div>

    <!-- スライドショー -->
    <div class="slider">
      <div id="theTarget">
        <div style="background-image: url(images/slide02.jpg);"></div>
        <div style="background-image: url(images/slide01.jpg);"></div>
        <div style="background-image: url(images/slide03.jpg);"></div>
      </div>
    </div>


    <!-- サイト紹介 -->
    <div class="home-content content_wrapper">
      <h2>タスクを"達成"することに重点を</h2>
      <p>
        この度は、当サイト「Complete」にお越しいただきありがとうございます。<br>
        当サイトでは、タスク管理サービスを提供いたします。タスクを管理するサービスは世の中にたくさんありますが、その中でも「Complete」では立てたタスクをいかに達成するかという点に力を入れています。<br>
        あなたもこれを機に立てたタスクをしっかりと達成するという習慣を身に着けていきませんか？<br>
        「Complete」の使い方は<a href="about.php"><span class="under_blue">ABOUT</span></a>をご覧ください。
      </p>
    </div>


    <!-- コンテンツ -->
    <div class="categories content_wrapper">
      <h2>Contents</h2>
      <div class="flex-wrapper">
        <a href="login.php">
          <div class="flex-inner">
            <p>無料会員登録<br><span style="font-size: 18px;">or</span><br>ログイン</p>
          </div>
        </a>
        <a href="mypage.php">
        <div class="flex-inner">
          <p>タスクを登録</p>
        </div>
        </a>
        <a href="mypage.php">
        <div class="flex-inner">
          <p>タスクを管理</p>
        </div>
        </a>
      </div>
    </div>


    <!-- フッター -->
    <footer class="footer">
      <div class="social">
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
        <a href="#"><i class="fab fa-youtube"></i></a>
      </div>
      <small>&copy;Complete</small>
    </footer>

    <!-- スクリプト -->
    <script type="text/javascript" src="js/script.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.skippr.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/js/swiper.min.js"></script>
    <script>
    $(document).ready(function(){
      $("#theTarget").skippr();
    });

    $("#theTarget").skippr({
      // スライドタイプ ("fade" or "slide")
      transition :'fade',
      // 次のスライドまでの移行時間(単位ミリ秒)
      speed : 2000,
      // イージングの種類(http://easings.net/ja)
      easing :'easeOutQuart',
      // ナヴ・タイプ("block" or "bubble")
      navType :'block',
      // 子要素の種類("div" or "img")
      childrenElementType :'div',
      // 矢印の表示有無(trueで表示)
      arrows :false,
      // スライドショーの自動再生
      autoPlay :true,
      // 自動再生時のスライド移行時間(単位ミリ秒)
      autoPlayDuration : 4000,
      // 矢印キーの有効化
      keyboardOnAlways :false,
      // 一枚目のスライドに戻る矢印の表示の有無
      hclassePrevious :true,
    });
    </script>
  </body>
</html>
